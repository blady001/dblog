/**
 * Created by macblady on 07.04.2017.
 */
var express = require('express');
var router = express.Router();
var Article = require('../models/article');
var mongoose = require('mongoose');


router.get('/new', function(req, res) {
    if (req.user)
        res.render('new_article', { title: 'Dawid Brzyszcz Blog - Nowy wpis', user: req.user});
    else
        res.send(401)
});

router.post('/new', function(req, res) {
    if (!req.user)
        res.send(401);

    var date = Date.now();
    var author = req.user.firstname + ' ' + req.user.lastname;

    var new_article = new Article({
        title: req.body.articleTitle,
        content: req.body.articleContent,
        created: date,
        updated: date,
        author: author
    });

    new_article.save(function(err, obj) {
        if (err) throw err;

        res.redirect('/articles/' + obj.id);
    });
});

router.get('/:articleId', function(req, res) {
    Article.findById(req.params.articleId, function(err, article) {
        if (err) throw err;
        // console.log(article);
        var data = {article: article, title: article.title };
        if (req.user)
            data['user'] = req.user;
        res.render('article', data);
    });
});

router.get('/:articleId/edit', function(req, res) {
    if (!req.user)
        res.render(401);

    Article.findById(req.params.articleId, function(err, article) {
        if (err) throw err;

        res.locals.articleTitle = article.title;
        res.locals.articleContent = article.content;
        res.render('edit_article', {user: req.user, article: article});

    })
});

router.post('/:articleId/edit', function (req, res) {
    if (!req.user)
        res.render(401);

    var condition = {_id: req.params.articleId};

    var updateClause = {
        title: req.body.articleTitle,
        content: req.body.articleContent,
        updated: Date.now()
    };


    Article.findOneAndUpdate(condition, updateClause, null, function(err, article) {
        if (err) throw err;

        res.redirect('/articles/' + req.params.articleId);
    });
});

router.get('/:articleId/delete', function(req, res) {
    if (!req.user)
        res.render(401);

    res.render('deletion_confirmation', {user: req.user, articleId: req.params.articleId});
});

router.post('/:articleId/delete', function(req, res) {
    if (!req.user)
        res.render(401);

    var condition = {_id: req.params.articleId};


    Article.deleteOne(condition, function(err, result) {
        if (err) throw err;
        res.redirect('/');
    });
});

module.exports = router;
