/**
 * Created by macblady on 07.04.2017.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var Article = new Schema({
    title: String,
    content: String,
    updated: { type: Date, default: Date.now },
    created: { type: Date },
    author: String
});

module.exports = mongoose.model('Article', Article);
