/**
 * Created by macblady on 24.04.2017.
 */

var express = require('express');
var router = express.Router();
var multer = require('multer');


var storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, __dirname + '/../uploads');
    },
    filename: function(req, file, callback) {
        callback(null, Date.now() + file.originalname);
    }
});

var upload = multer({storage: storage}).single('uploadedFile');


router.get('/', function(req, res) {
    res.render('file_upload', { title: 'File upload'});
});

router.post('/', function(req, res, next) {
    upload(req, res, function(err) {
        if (err) {
            console.log(err);
            return res.end("Error while uploading a file");
        }
        res.end("File is uploaded");
    });
});

module.exports = router;
