var express = require('express');
var router = express.Router();
var passport = require('passport');
var Account = require('../models/account');
var Article = require('../models/article');


/* GET home page. */
router.get('/', function(req, res, next) {
  Article.find({}).sort('-created').exec(function(err, articles) {
    if (err) throw err;

    res.render('index', { title: 'Dawid Brzyszcz Blog', user: req.user , articles: articles});

  });
  // res.render('index', { title: 'Dawid Brzyszcz Blog', user: req.user });
});

router.get('/contact', function(req, res, next) {
  res.render('contact', { title: 'Dawid Brzyszcz Blog - Kontakt', user: req.user });
});

router.get('/signin', function(req, res, next) {
  if (req.user)
    res.redirect('/');
  else
    res.render('signin')
});

router.post('/signin', passport.authenticate('local'), function(req, res) {
  res.redirect('/');
});

router.get('/signout', function(req, res) {
  req.logout();
  res.redirect('/');
});


// additional internal registration
router.post('/register', function(req, res) {
  Account.register(new Account({
    username: req.body.username,
    firstname: req.body.firstname,
    lastname: req.body.lastname,
    email: req.body.email
  }), req.body.password, function(err, account) {
    if (err) {
      console.log(err);
      return res.send("Failed to create account");
    } else {
      return res.send("User created succesfully");
    }
  })
});

module.exports = router;
